﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace JpLabs.Extensions
{
	public static class EnumExt
	{
		public static IEnumerable<T> GetValues<T>() //where T : Enum
		{
			return (T[])Enum.GetValues(typeof(T));
		}
	}
}
