﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections;

namespace JpLabs.Extensions
{
	public static class EmptyReadOnlyColl<T>
	{
		public static readonly ICollection<T> Value = Array.AsReadOnly(new T[0]);
	}

	public static class SpecificEnumerableExt
	{
		/// <summary>This is an optimized version of Count() for ICollection types</summary>
		public static bool Any<T>(this ICollection<T> source)
		{
			if (source == null) throw new ArgumentNullException("source");			
			return source.Count > 0;
		}

		/// <summary>Verify if an ICollection is null or is empty</summary>
		public static bool IsEmpty<T>(this ICollection<T> source)
		{
			return !source.Any();
		}

		public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> items)
		{
			if (collection == null) throw new ArgumentNullException("collection");
			if (items == null) throw new ArgumentNullException("items");
			
			foreach (var i in items) collection.Add(i);
		}

		/// <summary>This is an optimized version of ElementAt() for IList types</summary>
		public static T ElementAt<T>(this IList<T> source, int index)
		{
			if (source == null) throw new ArgumentNullException("source");
			
			return source[index]; //might throw ArgumentOutOfRangeException
		}

		/// <summary>This is an optimized version of ElementAtOrDefault() for IList types</summary>
		public static T ElementAtOrDefault<T>(this IList<T> source, int index)
		{
			if (source == null) throw new ArgumentNullException("source");
			
			if (index >= source.Count) return default(T);
			
			return source[index];
		}

		/// <summary>This is an optimized version of Last() for IList types</summary>
		public static T Last<T>(this IList<T> source)
		{
			if (source == null) throw new ArgumentNullException("source");
			
			int count = source.Count;
			if (count == 0) throw new InvalidOperationException("The source list is empty");
			
			return source[source.Count - 1];
		}

		/// <summary>This is an optimized version of LastOrDefault() for IList types</summary>
		public static T LastOrDefault<T>(this IList<T> source)
		{
			if (source == null) throw new ArgumentNullException("source");
			
			int count = source.Count;
			if (count == 0) return default(T);
			
			return source[source.Count - 1];
		}
	}

	public static class EnumerableExt
	{
		/// <summary>Converts a value to a single-element enumerable</summary>
		public static IEnumerable<T> ToEnumerable<T>(this T item)
		{
			return new [] { item }; //yield return item;
		}
		
		/// <summary>Creates an enumerable from zero or more parameters</summary>
		public static IEnumerable<T> YieldMany<T>(params T[] items)
		{
			return items;
		}

		/// <summary>Verify if an enumerable is null or is empty</summary>
		public static bool IsEmpty<T>(this IEnumerable<T> source)
		{
			return !source.Any();
		}
		
		/// <summary>Verify if an enumerable is null or is empty</summary>
		public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
		{
			return source == null || !source.Any();
		}

		/// <summary>Returns an empty enumerable if the source is null</summary>
		public static IEnumerable<T> EmptyIfNull<T>(this IEnumerable<T> source)
		{
			return source ?? Enumerable.Empty<T>();
		}

		//[Obsolete("Consider this: http://blogs.msdn.com/ericlippert/archive/2009/05/18/foreach-vs-foreach.aspx")]
		public static void ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource> action)
		{
			foreach (var o in source) action(o);
		}

		public static void ForEach(this IEnumerable source, Action<object> action)
		{
			foreach (var o in source) action(o);
		}
		
		public static ICollection<T> ToReadOnlyColl<T>(this IEnumerable<T> source)
		{
			if (source == null) throw new ArgumentNullException("source");
			
			if (source is ReadOnlyCollection<T>) return (ReadOnlyCollection<T>)source;
			
			if (source is T[]) return Array.AsReadOnly((T[])source);
			
			return Array.AsReadOnly(source.ToArray());
		}
		
		public static bool TrySingle<T>(this IEnumerable<T> source, out T singleValue)
		{
			if (source == null) throw new ArgumentNullException("source");
			
			singleValue = default(T);
						
			using (var it = source.GetEnumerator())
			{
				if (!it.MoveNext()) return false;
				singleValue = it.Current;
				if (it.MoveNext()) return false;
			}
			
			return true;
		}

		public static IEnumerable<TSource> CloneAll<TSource>(this IEnumerable<TSource> source) where TSource : ICloneable
		{
			return source.Select( c => (TSource)(c.Clone()) );
		}
		
		public static IEnumerable<TSource> CloneAllCloneable<TSource>(this IEnumerable<TSource> source)
		{
			return source.Select( c => !(c is ICloneable) ? default(TSource) : (TSource)((ICloneable)c).Clone() );
		}
		
		/// <summary>Regular Concat method taking advantage of the params keyword</summary>
		public static IEnumerable<T> Concat<T>(this IEnumerable<T> source, params T[] other)
		{
			return source.Concat((IEnumerable<T>)other);
		}

		/// <summary>Regular Union method taking advantage of the params keyword</summary>
		public static IEnumerable<T> Union<T>(this IEnumerable<T> source, params T[] other)
		{
			return source.Union((IEnumerable<T>)other);
		}
		
		/*/// <summary>Inverted Contains method extending the item, instead of the list</summary>
		public static bool In<T>(this T source, params T[] list)
		{
			if (source == null) throw new ArgumentNullException("source");
			return list.Contains(source);
		}//*/
		
		/*/// <summary>Concat method optimized for a single item</summary>
		public static IEnumerable<TSource> Concat<TSource>(this IEnumerable<TSource> source, TSource item)
		{
			if (source == null) throw new ArgumentNullException("source");
			
			return ConcatIterator<TSource>(source, item);
		}

		private static IEnumerable<TSource> ConcatIterator<TSource>(IEnumerable<TSource> source, TSource item)
		{
			foreach (var o in source) yield return o;
			yield return item;
		}//*/
		
		/// <summary>Determines if a set contains another</summary>
		public static bool Contains<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			ICollection<TSource> firstAsCollection = first as ICollection<TSource>;
			if (firstAsCollection != null) {
				if (second == null) throw new ArgumentNullException("second");
				return second.All(value => firstAsCollection.Contains(value));
			}
			
			return Contains<TSource>(first, second, null);
		}
		
		/// <summary>Determines if a set contains another</summary>
		public static bool Contains<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			if (comparer == null) comparer = EqualityComparer<TSource>.Default;
			if (first == null) throw new ArgumentNullException("first");
			if (second == null) throw new ArgumentNullException("second");
			
			return second.All(value => first.Contains(value, comparer));
		}

		/// <summary>FirstOrDefault overload where you can choose the default value to be used</summary>
		public static TSource FirstOrDefault<TSource>(this IEnumerable<TSource> source, TSource defaultValue)
		{
			if (source == null) throw new ArgumentNullException("source");
			
			IList<TSource> list = source as IList<TSource>;
			if (list != null) {
				if (list.Count > 0) return list[0];
			} else {
				using (IEnumerator<TSource> enumerator = source.GetEnumerator()) {
					if (enumerator.MoveNext()) return enumerator.Current;
				}
			}
			return defaultValue;
		}

		/// <summary>FirstOrDefault overload where you can choose the default value to be used</summary>
		public static TSource FirstOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate, TSource defaultValue)
		{
			if (source == null) throw new ArgumentNullException("source");
			if (predicate == null) throw new ArgumentNullException("predicate");
			
			foreach (TSource local in source) if (predicate(local)) return local;
			return defaultValue;
		}

		#if NET40
		/// <summary>
		/// http://community.bartdesmet.net/blogs/bart/archive/2008/11/03/c-4-0-feature-focus-part-3-intermezzo-linq-s-new-zip-operator.aspx
		/// http://blogs.msdn.com/ericlippert/archive/2009/05/07/zip-me-up.aspx
		/// </summary>
		public static IEnumerable<TResult> Zip<TFirst, TSecond, TResult>
			(this IEnumerable<TFirst> first,
			IEnumerable<TSecond> second,
			Func<TFirst, TSecond, TResult> resultSelector)
		{
			if (first == null) throw new ArgumentNullException("first");
			if (second == null) throw new ArgumentNullException("second");
			if (resultSelector == null) throw new ArgumentNullException("resultSelector");
			return ZipIterator(first, second, resultSelector);
		}

		private static IEnumerable<TResult> ZipIterator<TFirst, TSecond, TResult>
			(IEnumerable<TFirst> first,
			IEnumerable<TSecond> second,
			Func<TFirst, TSecond, TResult> resultSelector)
		{
			using (IEnumerator<TFirst> e1 = first.GetEnumerator()) {
				using (IEnumerator<TSecond> e2 = second.GetEnumerator()) {
					while (e1.MoveNext() && e2.MoveNext()) {
						yield return resultSelector(e1.Current, e2.Current);
					}
				}
			}
		}
		#endif

		////This is the same as Intersperse
		//public static T AggregateWithSeparators<T>(this IEnumerable<T> source, T separator, Func<T,T,T> aggregate)
		//{
		//    return AggregateWithSeparators(source, separator, default(T), aggregate);
		//}
		//public static T AggregateWithSeparators<T>(this IEnumerable<T> source, T separator, T seed, Func<T,T,T> aggregate)
		//{
		//    return
		//        source
		//        .Select((s,i) => (i==0) ? s : aggregate(separator, s))
		//        .Aggregate(seed, (s1,s2) => aggregate(s1,s2));
		//}

		/// <summary>Repeatedly yields element between each of the elements in the source</summary>
		public static IEnumerable<T> Intersperse<T>(this IEnumerable<T> source, T element)
		{
			if (source == null) throw new ArgumentNullException("source");
			
			return IntersperseIterator<T>(source, element);
		}
		
		private static IEnumerable<T> IntersperseIterator<T>(this IEnumerable<T> source, T element)
		{
			using(var i = source.GetEnumerator()) {
				if (i.MoveNext()) yield return i.Current;
				while(i.MoveNext()) {
					yield return element;
					yield return i.Current;
				}
			}
		}
		
		/// <summary>Joins strings insering a separator between each of them</summary>
		public static string Join(this IEnumerable<string> source, string separator)
		{
			//return source.Intersperse(separator, null, string.Concat);
			return string.Join(separator, source.ToArray());
		}
		
		/// <summary>http://en.wikipedia.org/wiki/Fisher-Yates_shuffle</summary>
		public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
		{
			return Shuffle(source, null);
		}

		public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source, Random rnd)
		{
			if (source == null) throw new ArgumentNullException("source");
			if (rnd == null) rnd = new Random();

			return ShuffleIterator(source, rnd);
		}

		static private IEnumerable<T> ShuffleIterator<T>(this IEnumerable<T> source, Random rnd)
		{
			T[] array = source.ToArray();
			for (int n = array.Length; n > 1;)
			{
				int k = rnd.Next(n--); // 0 <= k < n
		
				//Swap items
				if (n != k)
				{
					T tmp = array[k];
					array[k] = array[n];
					array[n] = tmp;
				}
			}
	
			foreach (var item in array) yield return item;
		}

		/// <summary>http://en.wikipedia.org/wiki/Topological_sorting</summary>
		public static IEnumerable<TSource> TopologicalOrderBy<TSource, TKey>(
			this IEnumerable<TSource> source,
			Func<TSource, TKey> keySelector)
		{
			return TopologicalOrderBy(source, keySelector, null);
		}

		/// <summary>http://en.wikipedia.org/wiki/Topological_sorting</summary>
		public static IEnumerable<TSource> TopologicalOrderBy<TSource, TKey>(
			this IEnumerable<TSource> source,
			Func<TSource, TKey> keySelector,
			IComparer<TKey> comparer)
		{
			if (source == null) throw new ArgumentNullException("source");
			if (keySelector == null) throw new ArgumentNullException("keySelector");
			if (comparer == null) comparer = (IComparer<TKey>)Comparer<TKey>.Default;
			
			return TopologicalOrderByIterator(source, keySelector, comparer);
		}

		private static IEnumerable<TSource> TopologicalOrderByIterator<TSource, TKey>(
			IEnumerable<TSource> source,
			Func<TSource, TKey> keySelector,
			IComparer<TKey> comparer)
		{

			var values = source.ToArray();
			var keys = values.Select(keySelector).ToArray();
			int count = values.Length;
			
			var notYieldedIndexes = System.Linq.Enumerable.Range(0, count).ToArray();
			int valuesToGo = count;
			
			while (valuesToGo > 0)
			{
				//Start with first value not yielded yet
				int minIndex = notYieldedIndexes.First( i => i >= 0);
				
				//Find minimum value amongst the values not yielded yet
				for (int i=0; i<count; i++)
				if (notYieldedIndexes[i] >= 0)
				if (comparer.Compare(keys[i], keys[minIndex]) < 0) {
					minIndex = i;
				}
				
				//Yield minimum value and mark it as yielded
				yield return values[minIndex];
				notYieldedIndexes[minIndex] = -1;
				valuesToGo--;
			}
		}
	}
}
