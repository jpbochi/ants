using System;
using System.Linq;
using System.Collections.Generic;
using JpLabs.Extensions;

namespace Ants
{
	static class MyBot
	{
		public static void Main (string[] args) {
			new Ants().PlayGame(new AntBot());
		}
	}
	
	class AntBot: Bot 
	{
		private Random rnd = new Random();
		private HashSet<Ant> orderedAnts = new HashSet<Ant>();
		private HashSet<Location> blockedDestinations = new HashSet<Location>();

		public override void DoSetup(GameState state)
		{
			//...
		}

		public override void DoTurn(IGameState state)
		{
			StartTurn(state);

			MoveFoodGatherers(state);
			
			MoveAntsAwayFromOthersOrRandom(state);
		}

		private void MoveAntsAwayFromOthersOrRandom(IGameState state)
		{
			int radiusToConsider = state.ViewRadius2;//(int)Math.Ceiling(Math.Sqrt(state.ViewRadius2) * 4);

			var antsToConsider = state.MyAnts;//.Except(orderedAnts).ToList();

			foreach (var ant in state.MyAnts.Except(orderedAnts)) {

				var closeAnts =
					antsToConsider
					.Where(other => state.GetWalkDistance(ant, other) < radiusToConsider)
					.ToList()
				;

				if (closeAnts.Any()) {
					var vectorDirection = closeAnts.Aggregate(new Vector(0, 0), (vec, other) => vec += state.GetDiff(other, ant).Denormalize());

					//vectorDirection =
					//	state.MyHills
					//	.Select(hill => hill.AsLocation())
					//	.Aggregate(vectorDirection, (vec, hill) => vec += state.GetDiff(ant, hill).Denormalize())
					//;

					var directions = vectorDirection.ToCardinal();

					if (!directions.Any(d => TryWalkAnt(state, ant, d))) {
						//TryMoveAntRandomly(state, ant);
					}
				} else {
					TryMoveAntRandomly(state, ant);
				}

				// check if we have time left to calculate more orders
				if (state.TimeRemaining < 10) break;
			}
		}

		private bool TryMoveAntRandomly(IGameState state, Ant ant)
		{
			// try all the directions in a random order
			foreach (Direction direction in EnumExt.GetValues<Direction>().Shuffle(rnd)) {
				var destination = state.GetDestination(ant, direction);

				if (TryWalkAnt(state, ant, direction)) {
					return true;
				}
			}
			return false;
		}

		private void StartTurn(IGameState state)
		{
			orderedAnts.Clear();
			blockedDestinations.Clear();
			blockedDestinations.AddRange(state.MyHills.Select(hill => hill.AsLocation()));
		}

		private void MoveFoodGatherers(IGameState state)
		{
			var foodsBeingGathered = new HashSet<Location>();

			int radiusToConsider = (int)Math.Ceiling(Math.Sqrt(state.ViewRadius2));

			foreach (var ant in state.MyAnts) {
				var closestFood =
					state.FoodTiles
					.Except(foodsBeingGathered)
					.Select(food =>	new { food, dist = state.GetWalkDistance(ant, food) })
					.OrderBy(food => food.dist)
					.FirstOrDefault();
				
				if (closestFood != null && closestFood.dist < radiusToConsider) {
					if (state.GetDirections(ant, closestFood.food).Any(
						direction => TryWalkAnt(state, ant, direction)
					)) {
						foodsBeingGathered.Add(closestFood.food);
					}
				}
			}
		}

		private bool TryWalkAnt(IGameState state, Ant ant, Direction direction)
		{
			return TryWalkAnt(state, ant, direction, state.GetDestination(ant, direction));
		}

		private bool TryWalkAnt(IGameState state, Ant ant, Direction direction, Location destination)
		{
			if (!state.GetIsUnoccupied(destination)) return false;
			if (!blockedDestinations.Add(destination)) return false;
			if (!orderedAnts.Add(ant)) return false;

			IssueOrder(ant, direction);
			return true;
		}
	}
}