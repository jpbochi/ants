using System;
using System.Collections.Generic;

namespace Ants {
	
	public class GameState : IGameState {
		
		public int Width { get; private set; }
		public int Height { get; private set; }
		
		public int LoadTime { get; private set; }
		public int TurnTime { get; private set; }
		
		private DateTime turnStart;
		public int TimeRemaining {
			get {
				TimeSpan timeSpent = DateTime.Now - turnStart;
				return TurnTime - timeSpent.Milliseconds;
			}
		}

		public int ViewRadius2 { get; private set; }
		public int AttackRadius2 { get; private set; }
		public int SpawnRadius2 { get; private set; }

		public ICollection<Ant> MyAnts { get; private set; }
		public ICollection<AntHill> MyHills { get; private set; }
		public List<Ant> EnemyAnts { get; private set; }
		public List<AntHill> EnemyHills { get; private set; }
		public List<Location> DeadTiles { get; private set; }
		public List<Location> FoodTiles { get; private set; }

		public Tile this[Location location] {
			get { return this.map[location.Row, location.Col]; }
		}

		public Tile this[int row, int col] {
			get { return this.map[row, col]; }
		}
		
		private Tile[,] map;
		
		public GameState (int width, int height, 
		                  int turntime, int loadtime, 
		                  int viewradius2, int attackradius2, int spawnradius2) {
			
			Width = width;
			Height = height;
			
			LoadTime = loadtime;
			TurnTime = turntime;
			
			ViewRadius2 = viewradius2;
			AttackRadius2 = attackradius2;
			SpawnRadius2 = spawnradius2;
			
			MyAnts = new List<Ant>();
			MyHills = new List<AntHill>();
			EnemyAnts = new List<Ant>();
			EnemyHills = new List<AntHill>();
			DeadTiles = new List<Location>();
			FoodTiles = new List<Location>();
			
			map = new Tile[height, width];
			for (int row = 0; row < height; row++) {
				for (int col = 0; col < width; col++) {
					map[row, col] = Tile.Land;
				}
			}
		}

		#region State mutators
		public void StartNewTurn () {
			// start timer
			turnStart = DateTime.Now;
			
			// clear ant data
			foreach (Location loc in MyAnts) map[loc.Row, loc.Col] = Tile.Land;
			foreach (Location loc in MyHills) map[loc.Row, loc.Col] = Tile.Land;
			foreach (Location loc in EnemyAnts) map[loc.Row, loc.Col] = Tile.Land;
			foreach (Location loc in EnemyHills) map[loc.Row, loc.Col] = Tile.Land;
			foreach (Location loc in DeadTiles) map[loc.Row, loc.Col] = Tile.Land;
			
			MyHills.Clear();
			MyAnts.Clear();
			EnemyHills.Clear();
			EnemyAnts.Clear();
			DeadTiles.Clear();
			
			// set all known food to unseen
			foreach (Location loc in FoodTiles) map[loc.Row, loc.Col] = Tile.Land;
			FoodTiles.Clear();
		}

		public void AddAnt (int row, int col, int team) {
			map[row, col] = Tile.Ant;
			
			Ant ant = new Ant(row, col, team);
			if (team == 0) {
				MyAnts.Add(ant);
			} else {
				EnemyAnts.Add(ant);
			}
		}

		public void AddFood (int row, int col) {
			map[row, col] = Tile.Food;
			FoodTiles.Add(new Location(row, col));
		}

		public void RemoveFood (int row, int col) {
			// an ant could move into a spot where a food just was
			// don't overwrite the space unless it is food
			if (map[row, col] == Tile.Food) {
				map[row, col] = Tile.Land;
			}
			FoodTiles.Remove(new Location(row, col));
		}

		public void AddWater (int row, int col) {
			map[row, col] = Tile.Water;
		}

		public void DeadAnt (int row, int col) {
			// food could spawn on a spot where an ant just died
			// don't overwrite the space unless it is land
			if (map[row, col] == Tile.Land) {
				map[row, col] = Tile.Dead;
			}
			
			// but always add to the dead list
			DeadTiles.Add(new Location(row, col));
		}

		public void AntHill (int row, int col, int team) {

			if (map[row, col] == Tile.Land) {
				map[row, col] = Tile.Hill;
			}

			AntHill hill = new AntHill (row, col, team);
			if (team == 0)
				MyHills.Add (hill);
			else
				EnemyHills.Add (hill);
		}
		#endregion

		/// <summary>
		/// Gets whether <paramref name="location"/> is passable or not.
		/// </summary>
		/// <param name="location">The location to check.</param>
		/// <returns><c>true</c> if the location is not water, <c>false</c> otherwise.</returns>
		/// <seealso cref="GetIsUnoccupied"/>
		public bool GetIsPassable (Location location) {
			return map[location.Row, location.Col] != Tile.Water;
		}
		
		/// <summary>
		/// Gets whether <paramref name="location"/> is occupied or not.
		/// </summary>
		/// <param name="location">The location to check.</param>
		/// <returns><c>true</c> if the location is passable and does not contain an ant, <c>false</c> otherwise.</returns>
		public bool GetIsUnoccupied (Location location) {
			return GetIsPassable(location) && map[location.Row, location.Col] != Tile.Ant;
		}
		
		/// <summary>
		/// Gets the destination if an ant at <paramref name="location"/> goes in <paramref name="direction"/>, accounting for wrap around.
		/// </summary>
		/// <param name="location">The starting location.</param>
		/// <param name="direction">The direction to move.</param>
		/// <returns>The new location, accounting for wrap around.</returns>
		public Location GetDestination (Location location, Direction direction) {
			Location delta = Ants.Aim[direction];
			
			int row = (location.Row + delta.Row) % Height;
			if (row < 0) row += Height; // because the modulo of a negative number is negative

			int col = (location.Col + delta.Col) % Width;
			if (col < 0) col += Width;
			
			return new Location(row, col);
		}

		/// <summary>
		/// Gets the distance between <paramref name="from"/> and <paramref name="to"/>.
		/// </summary>
		/// <param name="from">The first location to measure with.</param>
		/// <param name="to">The second location to measure with.</param>
		/// <returns>The distance between <paramref name="from"/> and <paramref name="to"/></returns>
		public int GetWalkDistance(Location from, Location to) {
			int d_row = Math.Abs(from.Row - to.Row);
			d_row = Math.Min(d_row, Height - d_row);
			
			int d_col = Math.Abs(from.Col - to.Col);
			d_col = Math.Min(d_col, Width - d_col);
			
			return d_row + d_col;
		}

		public Vector GetDiff(Location from, Location to)
		{
			int drow = (to.Row - from.Row);
			if (drow > Height / 2) drow = Height - drow;
			else if (-drow > Height / 2) drow = Height - drow;

			int dcol = (to.Col - from.Col);
			if (dcol > Width / 2) dcol = Width - dcol;
			else if (-dcol > Width / 2) dcol = Width - dcol;

			return new Vector(drow, dcol);
		}

		/// <summary>
		/// Gets the closest directions to get from <paramref name="from"/> to <paramref name="to"/>.
		/// </summary>
		/// <param name="from">The location to start from.</param>
		/// <param name="to">The location to determine directions towards.</param>
		/// <returns>The 1 or 2 closest directions from <paramref name="from"/> to <paramref name="to"/></returns>
		public IEnumerable<Direction> GetDirections(Location from, Location to) {
			if (from.Row < to.Row) {
				if (to.Row - from.Row >= Height / 2)
					yield return Direction.North;
				if (to.Row - from.Row <= Height / 2)
					yield return Direction.South;
			}
			if (to.Row < from.Row) {
				if (from.Row - to.Row >= Height / 2)
					yield return Direction.South;
				if (from.Row - to.Row <= Height / 2)
					yield return Direction.North;
			}
			
			if (from.Col < to.Col) {
				if (to.Col - from.Col >= Width / 2)
					yield return Direction.West;
				if (to.Col - from.Col <= Width / 2)
					yield return Direction.East;
			}
			if (to.Col < from.Col) {
				if (from.Col - to.Col >= Width / 2)
					yield return Direction.East;
				if (from.Col - to.Col <= Width / 2)
					yield return Direction.West;
			}
		}
		
		public bool GetIsVisible(Location loc)
		{
			List<Location> offsets = new List<Location>();
			int squares = (int)Math.Floor(Math.Sqrt(this.ViewRadius2));
			for (int r = -1 * squares; r <= squares; ++r)
			{
				for (int c = -1 * squares; c <= squares; ++c)
				{
					int square = r * r + c * c;
					if (square < this.ViewRadius2)
					{
						offsets.Add(new Location(r, c));
					}
				}
			}
			foreach (Ant ant in this.MyAnts)
			{
				foreach (Location offset in offsets)
				{
					if ((ant.Col + offset.Col) == loc.Col &&
						(ant.Row + offset.Row) == loc.Row)
					{
								 return true;
					}
				}
			}
			return false;
		}

	}
}

