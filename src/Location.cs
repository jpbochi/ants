using System;
using System.Collections.Generic;

namespace Ants
{
	public class Vector
	{
		public int RowDiff { get; private set; }
		public int ColDiff { get; private set; }

		public Vector(int rowDiff, int colDiff) {
			this.RowDiff = rowDiff;
			this.ColDiff = colDiff;
		}

		public static Vector operator +(Vector v1, Vector v2)
		{
			return new Vector(
				v1.RowDiff + v2.RowDiff,
				v1.ColDiff + v2.ColDiff
			);
		}

		public Vector Denormalize()
		{
			if (RowDiff == 0 && ColDiff == 0) return new Vector(0, 0);

			double length = (RowDiff * RowDiff) + (ColDiff * ColDiff);
			
			double dr = RowDiff * 121f;
			double dc = ColDiff * 121f;

			return new Vector(
				(int)Math.Round(dr / length),
				(int)Math.Round(dc / length)
			);
		}

		public IEnumerable<Direction> ToCardinal()
		{
			if (RowDiff == 0 && ColDiff == 0) yield break;

			if (Math.Abs(RowDiff) > Math.Abs(ColDiff)) {
				yield return (RowDiff > 0) ? Direction.South : Direction.North;
				yield return (ColDiff > 0) ? Direction.East : Direction.West;
			} else {
				yield return (ColDiff > 0) ? Direction.East : Direction.West;
				yield return (RowDiff > 0) ? Direction.South : Direction.North;
			}
		}
	}

	public class Location : IEquatable<Location>
	{
		/// <summary>
		/// Gets the row of this location.
		/// </summary>
		public int Row { get; private set; }

		/// <summary>
		/// Gets the column of this location.
		/// </summary>
		public int Col { get; private set; }

		public Location (int row, int col) {
			this.Row = row;
			this.Col = col;
		}

		public override bool Equals (object obj) {
			if (ReferenceEquals (null, obj))
				return false;
			if (ReferenceEquals (this, obj))
				return true;
			if (obj.GetType() != typeof (Location))
				return false;

			return Equals ((Location) obj);
		}

		public bool Equals (Location other) {
			if (ReferenceEquals (null, other))
				return false;
			if (ReferenceEquals (this, other))
				return true;

			return other.Row == this.Row && other.Col == this.Col;
		}

		public override int GetHashCode()
		{
			unchecked {
				return (this.Row * 397) ^ this.Col;
			}
		}
	}

	public class TeamLocation : Location, IEquatable<TeamLocation> {
		/// <summary>
		/// Gets the team of this ant.
		/// </summary>
		public int Team { get; private set; }

		public TeamLocation (int row, int col, int team) : base (row, col) {
			this.Team = team;
		}

		public Location AsLocation()
		{
			return new Location(Row, Col);
		}

		public bool Equals(TeamLocation other) {
			return base.Equals (other) && other.Team == Team;
		}

		public override int GetHashCode()
		{
			unchecked {
				int result = this.Col;
				result = (result * 397) ^ this.Row;
				result = (result * 397) ^ this.Team;
				return result;
			}
		}
	}
	
	public class Ant : TeamLocation, IEquatable<Ant> {
		public Ant (int row, int col, int team) : base (row, col, team) {
		}

		public bool Equals (Ant other) {
			return base.Equals (other);
		}
	}

	public class AntHill : TeamLocation, IEquatable<AntHill> {
		public AntHill (int row, int col, int team) : base (row, col, team) {
		}

		public bool Equals (AntHill other) {
			return base.Equals (other);
		}
	}
}

